/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks

import scala.collection.mutable

/**
 * A `FunctionCache` wraps a function to ensure its evaluation on each input is only ever performed once. It can be
 * thought of as a lazy-evaluated `Map`, or as an extension of `lazy val` to `def` declarations. Whenever a
 * `FunctionCache` instance is applied to a `T` for the first time, the underlying function `f` is called. Before
 * returning, the result is stored. If the same `T` is passed to the instance again, the stored result is returned.
 */
case class FunctionCache[T, U] (f: T => U) extends (T => U) {

    val cache: mutable.Map[T, U] = mutable.Map.empty

    override def apply (x: T) : U = ???

}
