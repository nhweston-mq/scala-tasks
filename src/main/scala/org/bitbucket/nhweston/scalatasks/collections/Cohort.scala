/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks.collections

import org.bitbucket.nhweston.scalatasks.collections.Cohort.Student

/**
 * Represents a collection of students.
 *
 * @param students  the students in this cohort.
 */
case class Cohort (students: Iterable[Student]) {

    /**
     * Maps student IDs to students.
     */
    lazy val studentsById: Map[Int, Student] = ???

    /**
     * Maps each unit code to a set of students taking the unit.
     */
    lazy val studentsByUnit: Map[String, Set[Student]] = ???

}

object Cohort {

    /**
     * Represents a student.
     *
     * @param id        the student ID.
     * @param gpa       the GPA of the student.
     * @param units     a collection of all unit codes this student is currently taking.
     */
    case class Student (
        id: Int,
        gpa: BigDecimal,
        units: Iterable[String]
    )

}
