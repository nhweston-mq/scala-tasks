/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks

object Rewrite {

    /**
     * Rewrites `seq` according to `rules`. Each occurence of left sequence in a rule as a subsequence in `seq` must be
     * replaced with the respective right subsequence until no left sequences occur as subsequences in the result.
     */
    def rewrite[T] (seq: Seq[T], rules: Seq[(Seq[T], Seq[T])]) : Seq[T] = ???

}
