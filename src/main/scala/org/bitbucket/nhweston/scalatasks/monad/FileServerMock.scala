/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks.monad

import org.bitbucket.nhweston.scalatasks.monad.FileServerMock.{Directory, File}
import org.bitbucket.nhweston.scalatasks.monad.Result.{Error, Success}

/**
 * Represents a file server with a file system.
 */
class FileServerMock (root: Directory) {

    /**
     * Returns the directory at the given location, if it exists.
     */
    private def getDirectory (path: Seq[String]) : Result[Directory] = {
        def aux (
            dir: Directory,
            path: Seq[String]
        ) : Result[Directory] = {
            path match {
                case head +: tail => dir.resources get head match {
                    case Some (next: Directory) => aux (next, tail)
                    case Some (_: File) => Error (s"`$head` is a file")
                    case None => Error (s"`$head` does not exist")
                }
                case Nil => Success (dir)
            }
        }
        aux (root, path)
    }

    /**
     * Returns the file at the given location, if it exists.
     */
    private def getFile (path: Seq[String]) : Result[File] = ???

    /**
     * Lists the names of resources at the given location, if it exists. The first half of the resulting pair is a list
     * of directory names, and the second half is a list of file names.
     */
    def listResources (path: Seq[String]) : Result[(Seq[String], Seq[String])] = ???

    /**
     * Returns the data contained in the file at the given location, if it exists and the user is in the whitelist for
     * the file.
     */
    def read (path: Seq[String], user: String) : Result[Array[Byte]] = ???

}

object FileServerMock {

    sealed abstract class Resource {

        lazy val isDirectory: Boolean = {
            this match {
                case _: Directory => true
                case _: File => false
            }
        }

        lazy val isFile: Boolean = !isDirectory

    }

    case class Directory (resources: Map[String, Resource]) extends Resource {

        lazy val (directories, files): (Map[String, Directory], Map[String, File]) = {
            resources.foldLeft ((Map.empty[String, Directory], Map.empty[String, File])) {
                case ((ds, fs), (name, resource)) => resource match {
                    case d: Directory => (ds + (name -> d), fs)
                    case f: File => (ds, fs + (name -> f))
                }
            }
        }

    }

    case class File (
        data: Array[Byte],
        whitelist: Set[String]
    ) extends Resource

}
