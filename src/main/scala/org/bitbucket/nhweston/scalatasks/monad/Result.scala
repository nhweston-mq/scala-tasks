/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks.monad

sealed abstract class Result[+T] {

    /**
     * If this `Result` is a `Success`, applies `f` to the result value wrapped in the `Success`. If this `Result` is
     * an `Error`, returns the `Error` unmodified.
     */
    def >>=[U] (f: T => Result[U]) : Result[U] = ???

}

object Result {

    case class Success[T] (result: T) extends Result[T]
    case class Error[T] (msg: String) extends Result[Nothing]

}
