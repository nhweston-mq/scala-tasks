/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks

object Pairs {

    /**
     * Pairs each element of `seq1` with each element of `seq2`.
     */
    def pairs[T, U] (seq1: Seq[T], seq2: Seq[U]) : Seq[(T, U)] = ???

}
