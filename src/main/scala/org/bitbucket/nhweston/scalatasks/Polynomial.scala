/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks

/**
 * A polynomial function. The coefficient of the term with exponent `n` is given by `coeffs (n)`, or is 0 if `n` is
 * greater than the degree of the polynomial.
 */
class Polynomial[T: Numeric] (val coeffs: T*) extends (T => T) {

    override def apply (x: T) : T = ???

    lazy val degree: Int = coeffs.length

    /**
     * Returns a polynomial `p(x) = this(x) + other(x)`.
     */
    def + (other: Polynomial[T]) : Polynomial[T] = ???

    /**
     * Returns a polynomial `p(x) = this(x) - other(x)`.
     */
    def - (other: Polynomial[T]) : Polynomial[T] = ???

    /**
     * Returns a polynomial `p(x) = this(x) * other(x)`.
     */
    def * (other: Polynomial[T]) : Polynomial[T] = ???

    /**
     * Returns a polynomial `p(x) = other(this(x))`.
     */
    def ~ (other: Polynomial[T]) : Polynomial[T] = ???

    /**
     * Returns a string representation of this polynomial.
     */
    override lazy val toString: String = ???

}
