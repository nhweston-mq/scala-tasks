/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks

import org.bitbucket.nhweston.scalatasks.Condition._

/**
 * A tree-like data structure for representing conditions.
 *
 * A `Condition` is <i>normal</i> if it satisfies all of the following:
 * <ul>
 *     <li>Does not contain `Always` or `Never` nested within any other node.</li>
 *     <li>Every `Negation` node contains a `Raw` node as a children.</li>
 *     <li>An `AllOf` node does not contain another `AllOf` node as one of it children.</li>
 *     <li>An `AnyOf` node does not contain another `AnyOf` node as one of it children.</li>
 *     <li>The `n` parameter of an `AtLeast` node is between two and two less than the length of `conditions` (both
 *     inclusive).</li>
 * </ul>
 */
sealed abstract class Condition[-T] extends (T => Boolean) {

    override def apply (x: T) : Boolean = ???

    override lazy val toString: String = ???

    def and[T0 <: T] (other: Condition[T0]) : Condition[T0] = conj (this, other)
    def or[T0 <: T] (other: Condition[T0]) : Condition[T0] = disj (this, other)
    lazy val negate: Condition[T] = not (this)

}

/**
 * All definitions here should preserve normality. That is, each definition should return a normal `Condition` assuming
 * that their parameters are normal.
 */
object Condition {

    def always[T]: Condition[T] = ???
    def never[T]: Condition[T] = ???
    def not[T] (condition: Condition[T]) : Condition[T] = ???
    def conj[T] (c1: Condition[T], c2: Condition[T]) : Condition[T] = ???
    def disj[T] (c1: Condition[T], c2: Condition[T]) : Condition[T] = ???
    def allOf[T] (conditions: Seq[Condition[T]]) : Condition[T] = ???
    def anyOf[T] (conditions: Seq[Condition[T]]) : Condition[T] = ???
    def atLeast[T] (n: Int) (conditions: T*) : Condition[T] = ???
    def exactlyOne[T] (c1: Condition[T], c2: Condition[T]) : Condition[T] = ???
    def neitherOrBoth[T] (c1: Condition[T], c2: Condition[T]) : Condition[T] = ???
    def raw[T] (descriptor: String) (f: T => Boolean) : Condition[T] = ???

    object Always extends Condition[Any]
    object Never extends Condition[Any]
    case class Negation[-T] (condition: Condition[T]) extends Condition[T]
    case class AllOf[-T] (conditions: Seq[Condition[T]]) extends Condition[T]
    case class AnyOf[-T] (conditions: Seq[Condition[T]]) extends Condition[T]
    case class AtLeast[-T] (conditions: Seq[Condition[T]], n: Int) extends Condition[T]
    case class ExactlyOne[-T] (c1: Condition[T], c2: Condition[T]) extends Condition[T]
    case class NeitherOrBoth[-T] (c1: Condition[T], c2: Condition[T]) extends Condition[T]
    case class Raw[-T] (descriptor: String, f: T => Boolean) extends Condition[T]

}
