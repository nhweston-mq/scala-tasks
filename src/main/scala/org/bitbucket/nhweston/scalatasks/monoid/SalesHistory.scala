/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks.monoid

/**
 * A history of transactions over some period of time.
 *
 * @param orderIds      a sequence of order IDs in the period sorted by time of placement.
 * @param customerIds   a set of IDs of all customers who made at least one transaction during the period.
 * @param items         maps each item to the quantity sold during the period.
 */
case class SalesHistory (
    orderIds: Seq[Int],
    customerIds: Set[Int],
    items: Map[String, Int]
)

object SalesHistory {

    /**
     * Represents an order.
     *
     * @param orderId       the ID of the order.
     * @param customerId    the customer who placed the order.
     * @param items         maps each item to the quantity sold.
     */
    case class Order (
        orderId: Int,
        customerId: Int,
        items: Map[String, Int]
    )

    /**
     * Defines addition as a monoid for any numeric type.
     */
    def numericMonoid[T] (implicit numeric: Numeric[T]) : Monoid[T] = {
        import numeric._
        new Monoid[T] {
            override def zero: T = numeric.zero
            override def append (x1: T, x2: T) : T = x1 + x2
        }
    }

    /**
     * Defines concatenation as a monoid for any numeric type.
     */
    def seqMonoid[T]: Monoid[Seq[T]] = {
        new Monoid[Seq[T]] {
            override def zero: Seq[T] = ???
            override def append (seq1: Seq[T], seq2: Seq[T]) : Seq[T] = ???
        }
    }

    /**
     * Defines a monoid for any `Map` type whose value type also has a monoid defined.
     *
     * `append (map1, map2)` should merge two maps together. The resulting map should contain all entries contained by
     * either map. If both maps have a mapping for the same key, say `v1` in `map1` and `v2` in `map2`, the resulting
     * map should contain the key mapped to `append (v1, v2)`.
     */
    def mapMonoid[K, V] (implicit valueMonoid: Monoid[V]) : Monoid[Map[K, V]] = {
        new Monoid[Map[K, V]] {
            override def zero: Map[K, V] = ???
            override def append (map1: Map[K, V], map2: Map[K, V]) : Map[K, V] = ???
        }
    }

    /**
     * Defines a monoid for sales history, with `append` merging two histories together.
     */
    def salesHistoryMonoid: Monoid[SalesHistory] = {
        new Monoid[SalesHistory] {
            override def zero: SalesHistory = ???
            override def append (sh1: SalesHistory, sh2: SalesHistory) : SalesHistory = ???
        }
    }

    /**
     * Returns a sales history for the given orders, which are assumed to be all orders over some period of time,
     * sorted by time of placement.
     */
    def collectHistory (orders: Seq[Order]) : SalesHistory = ???

}
