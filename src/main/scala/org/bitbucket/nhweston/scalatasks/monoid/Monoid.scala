/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks.monoid

/**
 * A monoid is a group-like structure whose binary operation is total, associative, and unital. Unlike a group, a
 * monoid need not be invertible. Many, if not most data structures can be naturally characterised as monoids.
 */
trait Monoid[T] {

    /**
     * The identity element. `append (zero, x)` and `append (x, zero)` should both be equal to `x`.
     */
    def zero: T

    /**
     * The binary operation. The binary operation should be associative, that is, `append (a, append (b, c))` should be
     * equal to `append (append (a, b), c)`.
     */
    def append (x1: T, x2: T) : T

    /**
     * Returns the result of `append (append (… append (x) …))`, where `append` appears `n` times. However, the
     * implementation of this method should take advantage of monoidal properties so that the number of actual calls to
     * `append` is only O(log(n)).
     */
    def repeat (x: T, n: Int) : T = ???

    /**
     * Returns the resulting of appending every element of `xs` in sequence.
     */
    def join (xs: Seq[T]) : T = ???

}

object Monoid {

    import language.implicitConversions

    /**
     * Allows `x1 |+| x2` and `x1 |*| x2` to be used in place of `monoid.append (x1, x2)` and `monoid.join (x1, x2)`
     * respectively.
     */
    class MonoidOps[T] (self: T) (implicit monoid: Monoid[T]) {
        def |+| (other: T) : T = monoid.append (self, other)
        def |*| (n: Int) : T = monoid.repeat (self, n)
    }

    /**
     * Implicitly wraps a monoid element in `MonoidOps`.
     */
    implicit def toMonoidOps[T] (self: T) (implicit monoid: Monoid[T]) : MonoidOps[T] = {
        new MonoidOps[T] (self)
    }

}
