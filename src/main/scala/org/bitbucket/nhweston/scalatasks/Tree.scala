/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks

/**
 * A tree data structure where each node is either a leaf containing a value or a branch containing zero or more
 * subtrees.
 */
sealed abstract class Tree[T] {

    /**
     * Returns the first value in this tree satisfying `predicate` in a depth-first traversal of this tree.
     */
    def searchDepthFirst (predicate: T => Boolean) : Option[T] = ???

    /**
     * Returns the first value in this tree satisfying `predicate` in a breadth-first traversal of this tree.
     */
    def searchBreadthFirst (predicate: T => Boolean) : Option[T] = ???

}

object Tree {

    case class Branch[T] (children: Seq[T]) extends Tree[T]
    case class Leaf[T] (value: T) extends Tree[T]

}
