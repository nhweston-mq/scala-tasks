/*
 * Programming tasks for learning functional programming.
 * Copyright (C) 2019 Nicholas Weston.
 *
 * All rights reserved.
 */

package org.bitbucket.nhweston.scalatasks

/**
 * Unary natural numbers, as defined by Peano arithmetic.
 */
sealed abstract class Nat {

    /**
     * Addition of natural numbers.
     */
    def + (other: Nat) : Nat = ???

    /**
     * Truncated subtraction of natural numbers. Note that if `other` is greater than `this`, the result is `Zero`.
     */
    def - (other: Nat) : Nat = ???

    /**
     * Multiplication of natural numbers.
     */
    def * (other: Nat) : Nat = ???

    /**
     * Division of natural numbers. The first element of the resulting pair is the quotient, the second is the
     * remainder.
     */
    def /% (other: Nat) : (Nat, Nat) = ???

    def / (other: Nat) : Nat = {
        this /% other match {
            case (quotient, _) => quotient
        }
    }

    def % (other: Nat) : Nat = {
        this /% other match {
            case (_, remainder) => remainder
        }
    }

    /**
     * Comparison of natural numbers.
     */
    def < (other: Nat) : Boolean = ???

    def > (other: Nat) : Boolean = other < this

    def <= (other: Nat) : Boolean = ! (other < this)

    def >= (other: Nat) : Boolean = ! (this < other)

    /**
     * The `Int` value of this natural number.
     */
    lazy val toInt: Int = ???

}

object Nat {

    /** The successor to `n`, that is, `n + 1`. */
    case class Succ (n: Nat) extends Nat

    /** The first natural number, that is, `0`. */
    object Zero extends Nat

}
