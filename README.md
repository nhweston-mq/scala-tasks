# Programming tasks for learning functional programming

## Easy

- **Collections**: Common programming tasks in building collections from other collections.
- **FunctionCache**: Extending lazy evaluation to functions.
- **Pairs**: Creating a sequence of all possible pairs.

## Intermediate

- **Condition**: Representing conditions with a tree-like structure.
- **Generate**: Evaluating a recursive set definition.
- **Nat**: Unary natural numbers in the style of Peano arithmetic.
- **Polynomial**: Polynomial evaluation, addition, multiplication, and composition.
- **Rewrite**: Applying rewrite rules to a sequence of symbols.
- **Tree**: Recursive depth-first search and breadth-first search.

## Advanced

- **Monad**: Applying the monad pattern to error handling.
- **Monoid**: Applying abstract algebra to data structure operations.

Copyright (C) 2019 Nicholas Weston. All rights reserved.