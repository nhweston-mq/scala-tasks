ThisBuild/organization := "org.bitbucket.nhweston"
ThisBuild/scalaVersion := "2.12.8"
ThisBuild/scalacOptions := Seq (
    "-deprecation",
    "-feature",
    "-unchecked",
    "-Xcheckinit"
)

lazy val root = (project in file(".")) .settings (
    name := "scala-tasks",
    libraryDependencies ++= Seq (
        "org.scalatest" %% "scalatest" % "3.0.5" % "test"
    )
)